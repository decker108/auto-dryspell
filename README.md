# auto-dryspell

Description here

## Setup

1. Download telldus package key: `wget deb http://download.telldus.com/debian/ stable main`
2. Add key to apt: `sudo apt-key add telldus-public.key`
3. Add a new apt repo by running: `sudo nano /etc/apt/sources.list` and add the line `deb http://download.telldus.com/debian/ stable main`.
4. Compile from source using below guide(s).
5. Run tdtool -l and make sure it response with an example device.
6. Update /etc/tellstick.conf with the example file's config
7. Restart pi
8. Run tdtool -l

Note that every time the tellstick.conf is updated, the telldusd service must be restarted with `sudo service telldusd restart`.

You might also have to reprogram the Nexa switch:

1. Ensure that the device is in the tellstick.conf and model is
"selflearning-switch:nexa".
2. Put the Nexa device in a socket and connect a lamp to it.
3. Press the Nexa devices topside button once.
4. The LED above the button should start blinking.
5. Run the tdtool with the self-learning flag: `tdtool --learn 1` where 1
is the device id.
6. The lamp shold blink three times to confirm reprogramming.

### Compiling telldus-core from source (apt version)

    sudo sh -c 'echo "deb-src http://download.telldus.com/debian/ stable main" > /etc/apt/sources.list.d/telldus.list'
    wget http://download.telldus.com/debian/telldus-public.key
    sudo apt-key add telldus-public.key
    sudo apt-get update
    sudo apt-get install build-essential
    sudo apt-get build-dep telldus-core
    sudo apt-get install cmake libconfuse-dev libftdi-dev help2man
    mkdir -p ~/tellstick-build
    cd ~/tellstick-build
    sudo apt-get --compile source telldus-core
    sudo dpkg --install *.deb

### Compiling telldus-core from source (deb version)

    wget http://download.telldus.com/debian/telldus-public.key
    sudo apt-key add telldus-public.key

    sudo echo "deb-src https://s3.eu-central-1.amazonaws.com/download.telldus.com stable main" >> /etc/apt/sources.list.d/telldus-stable.list

    sudo apt-get update

    # Get packages needed for general builds
    sudo apt-get install build-essential fakeroot devscripts cmake

    # Needed to sucessfully run build of telldus-core but not specified as build deps
    sudo apt-get install libftdi-dev libconfuse-dev help2man

    # Get build deps that are specified in the package telldus core
    sudo apt-get build-dep telldus-core

    # Add the linker flag for pthread, otherwise linking will fail
    export DEB_LDFLAGS_APPEND='-pthread'

    # Get the source of telldus core
    apt-get source telldus-core

    cd telldus-core-2.1.3-beta1/
    # Build packages (unsigned)
    debuild -b -uc -us

    # In case the build complains about a missing Doxyfile.in, create an empty one
    touch Doxyfile.in

    # Install the newly built packages
    sudo dpkg -i libtelldus-core2_2.1.3-beta1-1_amd64.deb
    sudo dpkg -i telldus-core_2.1.3-beta1-1_amd64.deb

    tdtool --version
