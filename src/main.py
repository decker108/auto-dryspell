import sys
from tellstick_client import activateFan, deactivateFan


def main():
    inputArg = sys.argv[1]
    if inputArg == 'activate':
        print('activating fan')
        activateFan()
    elif inputArg == 'deactivate':
        print('deactivating fan')
        deactivateFan()
    else:
        print(f'Got unknown input argument: {inputArg}')


if __name__ == '__main__':
    main()
